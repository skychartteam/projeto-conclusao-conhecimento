
# Desafio Frontend
 Objetivo Desenvolver uma aplicação que possibilite realizar o check in de um hóspede em um hotel. 
 Não há necessidade de desenvolver o backend, você pode usar dados fixos em um JSON para a carga inicial e manter o restante dos dados em memória. 

###  Requisitos funcionais 

 - Um CRUD para o cadastro de pessoas; 
 - No check in deve ser possível buscar pessoas desse cadastro pelo nome ou documento;
 - Consultar pessoas que já realizaram o check in e não estão mais no hotel;
 - Consultar pessoas que ainda estão no hotel; 
 - As consultas devem mostrar o valor que a pessoa gastou no hotel e podem ficar na mesma página.

#### Estrutura do objeto pessoa

```json
{ 
	"cdPessoa": 1 ,
	"dsNome": "Fulano",
	"dsDocumento": "123456",
	"dsFone": "99123 4567"
} 
```

#### Estrutura do check in

```json
{ 
	"cdPessoa": 1,
	"dtEntrada": "2018-03-14T08:00:00",
	"dtSaida": "2018-03-16T10:17:00",
	"idVeiculo": true/false 
} 
```
* data no padrão ISO-8601

### Regras de negócio 

 - Uma diária no hotel de segunda à sexta custa R$120,00; 
 - Uma diária no hotel em finais de semana custa R$150,00; 
 - Caso a pessoa precise de uma vaga na garagem do hotel há um acréscimo diário, sendo R$15,00 de segunda à sexta e R$20,00 nos finais de semana;
 - Caso o horário da saída seja após às 16:30h deve ser cobrada uma diária extra. 